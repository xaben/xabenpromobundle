<?php

namespace Xaben\PromoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;

class PromoAdmin extends Admin
{
    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     *//*
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title')
            ->add('added')
            ->add('url')
        ;
    }*/

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General')
                //->add('category', null, array('required' => true))
                ->add('title', null, array('required' => false))
                ->add('description', null, array('required' => false))
                //->add('added', null, array('required' => true))
                ->add('url', null, array('required' => false))
                ->add('media', 'sonata_type_model_list', array(), array('link_parameters' => array('context' => 'promo')))
                ->add('position', null, array('required' => true))
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     *//*
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('_action', 'actions', array(
                    'actions' => array(
                            'edit' => array(),
                            'delete' => array(),
                    )
            ))
            ->add('added')
            ->addIdentifier('title')
            ->add('url')
        ;
    }*/

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     *//*
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('added')
            ->add('url')
        ;
    }*//*
    public function getNewInstance()
    {
        $promo = parent::getNewInstance();

        if ($this->hasRequest()) {
            $request = $this->getRequest()->query->all();
            $category = $this->getModelManager()->find('XabenPromoBundle:Category', $request['objectId']);
            die(print_r($category));
            $promo->setCategory($category);
        }

        return $promo;
    }*/
}
