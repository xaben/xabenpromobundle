<?php

namespace Xaben\PromoBundle\Provider;

interface PromoProviderInterface
{
    public function getTemplate();

    public function getCSS();

    public function getJS();

    public function init($box, $settings);
}