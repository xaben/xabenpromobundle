<?php

namespace Xaben\PromoBundle\Provider;

use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

class ProviderPool
{
    private $providers;

    public function __construct()
    {
        $this->providers = array();
    }

    public function addProvider(PromoProviderInterface $provider, $alias = null)
    {
        $this->providers[$alias] = $provider;
    }

    public function getProvider($alias)
    {
        if (array_key_exists($alias, $this->providers)) {
            return $this->providers[$alias];
        }

        $alternatives = array_keys($this->providers);
        throw new ServiceNotFoundException($alias, null, null, $alternatives);
    }
}