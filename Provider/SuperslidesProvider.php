<?php

namespace Xaben\PromoBundle\Provider;

class SuperslidesProvider implements PromoProviderInterface
{
    public function getTemplate(){
        return 'XabenPromoBundle:Sliders:superslides.html.twig';
    }

    public function getCSS(){
        return array(
            'superslides/superslides.css'
        );
    }

    public function getJS(){
        return array(
            'superslides/jquery.superslides.min.js'
        );
    }

    public function init($box, $settings){

    return "$(function() {
        $('#slides').superslides({
            inherit_width_from: '.wide-container',
            inherit_height_from: '.wide-container',
            play: 5000
        })
        });";

    }
}