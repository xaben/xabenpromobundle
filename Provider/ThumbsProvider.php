<?php

namespace Xaben\PromoBundle\Provider;

class ThumbsProvider implements PromoProviderInterface
{
    public function getTemplate(){
        return 'XabenPromoBundle:Sliders:thumbs.html.twig';
    }

    public function getCSS(){

    }

    public function getJS(){

    }

    public function init($box, $settings){

    }
}