<?php

namespace Xaben\PromoBundle\Provider;

class BootstrapCarouselProvider implements PromoProviderInterface
{
    public function getTemplate(){
        return 'XabenPromoBundle:Sliders:bootstrapCarousel.html.twig';
    }

    public function getCSS(){
        return [];
    }

    public function getJS(){
        return [];
    }

    public function init($box, $settings){
        return '';
    }
}
