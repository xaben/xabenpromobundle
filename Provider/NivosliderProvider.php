<?php

namespace Xaben\PromoBundle\Provider;

class NivosliderProvider implements PromoProviderInterface
{
    public function getTemplate(){
        return 'XabenPromoBundle:Sliders:nivoslider.html.twig';
    }

    public function getCSS(){

    }

    public function getJS(){

    }

    public function init($box, $settings){

    }
}