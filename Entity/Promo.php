<?php

namespace Xaben\PromoBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Promo
 *
 * @ORM\Table(name="promo_promo")
 * @ORM\Entity(repositoryClass="Xaben\PromoBundle\Entity\PromoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Promo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = "3",
     *      max = "255"
     * )
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="added", type="datetime")
     */
    private $added;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     * @Assert\Url()
     * @Assert\Length(
     *      min = "3",
     *      max = "255"
     * )
     */
    private $url;

    /**
    *
    * @ORM\ManyToOne(targetEntity="Category", inversedBy="promos")
    * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
    *
    */
    private $category;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     * @Assert\Type(type="integer")
     */
    private $position;

    /**
    *
    * @ORM\OneToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"all"}, orphanRemoval=true)
    *
    */
    private $media;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param  string $title
     * @return Promo
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param  string $description
     * @return Promo
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set added
     *
     * @param  DateTime $added
     * @return Promo
     */
    public function setAdded($added)
    {
        $this->added = $added;

        return $this;
    }

    /**
     * Get added
     *
     * @return DateTime
     */
    public function getAdded()
    {
        return $this->added;
    }

    /**
     * Set url
     *
     * @param  string $url
     * @return Promo
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set position
     *
     * @param  integer $position
     * @return Promo
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set category
     *
     * @param  \Xaben\PromoBundle\Entity\Category $category
     * @return Promo
     */
    public function setCategory(\Xaben\PromoBundle\Entity\Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Xaben\PromoBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set media
     *
     * @param  \Application\Sonata\MediaBundle\Entity\Media $media
     * @return Promo
     */
    public function setMedia(\Application\Sonata\MediaBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreationTime()
    {
        $this->setAdded(new DateTime());
    }
}
