<?php

namespace Xaben\PromoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Category
 *
 * @ORM\Table(name="promo_category")
 * @ORM\Entity(repositoryClass="Xaben\PromoBundle\Entity\CategoryRepository")
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = "3",
     *      max = "255"
     * )
     */
    private $title;

    /**
    *
    * @ORM\OneToMany(targetEntity="Promo", mappedBy="category", cascade={"all"}, orphanRemoval=true)
    * @ORM\OrderBy({"position" = "ASC"})
    *
    */
    private $promos;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->promos = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->title ? $this->title : '';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param  string   $title
     * @return Category
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add promos
     *
     * @param  \Xaben\PromoBundle\Entity\Promo $promos
     * @return Category
     */
    public function addPromo(\Xaben\PromoBundle\Entity\Promo $promos)
    {
        $promos->setCategory($this);
        $this->promos[] = $promos;

        return $this;
    }

    /**
     * Remove promos
     *
     * @param \Xaben\PromoBundle\Entity\Promo $promos
     */
    public function removePromo(\Xaben\PromoBundle\Entity\Promo $promos)
    {
        $this->promos->removeElement($promos);
    }

    /**
     * Get promos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPromos()
    {
        return $this->promos;
    }
}
