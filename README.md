XabenPromoBundle
========================

A simple bundle that integrates multiple sliders, with flexible configuration options.

Complete documentation will follow later on here.

1. Install bundle with composer
2. Load bundle in AppKernel
3. Update Doctrine schema
4. Add media definition to sonata media
```yaml
sonata_media:
    default_context: default
    db_driver: doctrine_orm # or doctrine_mongodb
    contexts:
        ...
        promo:
            providers:
                - sonata.media.provider.image
            formats:
                big: { height: 500, quality: 80 }
```
5. Then 

Render slider:
{{ xaben_promos('superslides','slider', 'promo') }}

Init JS:
{{ xaben_promos_init('superslides', 'slider') }}

Load JS:
{{ xaben_promos_js('superslides') }}
Load CSS:
{{ xaben_promos_css('superslides') }}
