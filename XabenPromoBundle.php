<?php

namespace Xaben\PromoBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

use Xaben\PromoBundle\DependencyInjection\Compiler\PromoProviderCompilerPass;

class XabenPromoBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new PromoProviderCompilerPass());
    }
}
