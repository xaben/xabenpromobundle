<?php

namespace Xaben\PromoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function nivoAction($category = 'mainpromo')
    {
        $em = $this->getDoctrine()->getManager();
        $promos = $em->getRepository('XabenPromoBundle:Category')
                     ->getPromos($category);

        return $this->render('XabenPromoBundle:Default:nivo.html.twig', array('promos' => $promos));
    }

    public function thumbsAction($category = 'smallpromo')
    {
        $em = $this->getDoctrine()->getManager();
        $promos = $em->getRepository('XabenPromoBundle:Category')
                     ->getPromos($category);

        return $this->render('XabenPromoBundle:Default:thumbs.html.twig', array('promos' => $promos));
    }
}
