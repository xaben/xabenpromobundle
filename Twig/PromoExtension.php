<?php
namespace Xaben\PromoBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;

class PromoExtension extends \Twig_Extension
{

    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return array(
            'xaben_promos' => new \Twig_Function_Method($this, 'getPromos', array('is_safe' => array('html'))),
            'xaben_promos_init' => new \Twig_Function_Method($this, 'initPromos', array('is_safe' => array('html'))),
            'xaben_promos_js' => new \Twig_Function_Method($this, 'JSassets', array('is_safe' => array('html'))),
            'xaben_promos_css' => new \Twig_Function_Method($this, 'CSSassets', array('is_safe' => array('html'))),
        );
    }

    /**
     * Create the main html for the promo
     *
     * @param $sliderName
     * @param $box
     * @param $category
     */
    public function getPromos($sliderName, $box, $category)
    {
        $promoalbum = $this->container->get('doctrine.orm.entity_manager')->getRepository('XabenPromoBundle:Category')->getPromos($category);
        $slider = $this->container->get('xaben.promo.provider_pool')->getProvider($sliderName);

        return $this->container->get('templating')->render($slider->getTemplate(),
            array('box' => $box, 'promoalbum' => $promoalbum, 'category' => $category));
    }

    /**
     * Initializes the slider JS onLoad
     *
     * @param $box Div ID to use
     * @param $sliderName
     * @param $settings
     */
    public function initPromos($sliderName, $box, $settings = array())
    {
        $slider = $this->container->get('xaben.promo.provider_pool')->getProvider($sliderName);
        return $slider->init($box, $settings);
    }

    /**
     * Return list of CSS assets used by slider
     *
     * @param $sliderName
     * @return string
     */
    public function CSSassets($sliderName)
    {
        $css = $this->container->get('xaben.promo.provider_pool')->getProvider($sliderName)->getCSS();
        $output = '';
        $assetHelper = $this->container->get('templating.helper.assets');

        foreach ($css as $asset) {
            $output .= '<link rel="stylesheet" href="' . $assetHelper->getUrl('/bundles/xabenpromo' . $asset,
                    null) . '" />';
        }

        return $output;
    }

    /**
     * Return list of JS used by slider
     *
     * @param $sliderName
     * @return string
     */
    public function JSassets($sliderName)
    {
        $js = $this->container->get('xaben.promo.provider_pool')->getProvider($sliderName)->getJS();
        $output = '';
        $assetHelper = $this->container->get('templating.helper.assets');

        foreach ($js as $asset) {
            $output .= '<script type="text/javascript" src="' . $assetHelper->getUrl('/bundles/xabenpromo/' . $asset,
                    null) . '"></script>';
        }

        return $output;
    }

    /**
     * @inherit()
     */
    public function getName()
    {
        return 'xaben_promo_extension';
    }
}
